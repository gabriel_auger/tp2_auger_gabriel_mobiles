package com.example.tp2_auger_gabriel

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tp2_auger_gabriel.databinding.FragmentHabitudesChoisiesBinding
import com.example.tp2_auger_gabriel.model.Category
import com.example.tp2_auger_gabriel.model.Habitude

class HabitudesChoisiesRecyclerViewAdapter(
    private var habitudes: List<Habitude>
) : RecyclerView.Adapter<HabitudesChoisiesRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentHabitudesChoisiesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun getItemCount(): Int = habitudes.size

    fun setData(newData: List<Habitude>) {
        habitudes = newData
        notifyDataSetChanged()
    }

    inner class ViewHolder(binding: FragmentHabitudesChoisiesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val icon: ImageView = binding.icon
        val title: TextView = binding.title
        val descriptionView: TextView = binding.description

        override fun toString(): String {
            return super.toString() + " '" + descriptionView.text + "'"
        }
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = habitudes[position]
        holder.title.text = item.name
        holder.descriptionView.text = item.description
        holder.icon.setImageResource(getIconResource(item.categorie))
    }

    private fun getIconResource(category: Category): Int {
        // Return the appropriate icon resource based on the category
        // This is just a placeholder. Replace with your actual logic.
        return R.drawable.baseline_settings_24
    }
}