package com.example.tp2_auger_gabriel

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.tp2_auger_gabriel.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val toolbar = findViewById<Toolbar>(R.id.tbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Mon titre"
        supportActionBar?.subtitle = "Mon sous-titre"
        supportActionBar?.setLogo(android.R.drawable.alert_dark_frame)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        val navController = findNavController(navHostFragment)


        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.listeHabitudes, R.id.habitudesChoisies
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Charger le menu à partir du fichier XML toolbar_menu.xml
        menuInflater.inflate(R.menu.toolbar, menu)
        return true
    }
}


