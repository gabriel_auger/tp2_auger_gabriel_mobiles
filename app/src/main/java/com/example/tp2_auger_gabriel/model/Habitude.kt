package com.example.tp2_auger_gabriel.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// Déclare une entité Room qui représente une table dans la base de données SQLite.
// "users" est le nom de la table que l'on souhaite créer.
@Entity(tableName = "habitudes")
data class Habitude(
    // Définit la colonne id comme clé primaire de la table et indique que cette valeur doit être générée automatiquement.
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    // Spécifie que le champ 'name' de cette classe doit être mappé à la colonne 'first_name' dans la table 'users'.
    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "description")
    val description: String = "Aucune description",

    @ColumnInfo(name = "choisie")
    var choisie: Boolean = false,

    @ColumnInfo(name = "Categorie")
    var categorie: Category = Category.HEALTH

    )

//companion object
{
    companion object {
        fun populateData(): List<Habitude> {
            return listOf(
                Habitude(1, "Faire 10 push-ups", "go tes capable"),
                Habitude(2, "Passer le balai", "Ne pas oublier la salle de bain"),
                Habitude(3, "Méditer 20 minutes")

            )
        }
    }
}

enum class Category {
    HEALTH,
    CLEANING,
    MEDITATION,
}