package com.example.tp2_auger_gabriel

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tp2_auger_gabriel.data.HabitudeDao
import com.example.tp2_auger_gabriel.databinding.FragmentListeHabitudesBinding
import com.example.tp2_auger_gabriel.model.Habitude
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HabitudesDispoRecyclerViewAdapter(
    private val values: List<Habitude>, private val listener: OnItemClickListener,
    private val dao : HabitudeDao
) : RecyclerView.Adapter<HabitudesDispoRecyclerViewAdapter.ViewHolder>() {
    interface OnItemClickListener {
        fun onItemClick(item: Habitude)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentListeHabitudesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.id.toString()
        holder.contentView.text = item.name
        holder.itemView.setOnClickListener {
            listener.onItemClick(item)
            var habitudeASauvegarder = Habitude(item.id, item.name, item.description, item.choisie)

            CoroutineScope(Dispatchers.IO).launch {
                dao.insert(habitudeASauvegarder)
            }
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentListeHabitudesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val idView: TextView = binding.itemNumber
        val contentView: TextView = binding.content

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }
}