package com.example.tp2_auger_gabriel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tp2_auger_gabriel.model.Habitude

class ViewModelPartage : ViewModel() {
    private val _selectedItems = MutableLiveData<List<Habitude>>(emptyList())

    val selectedItems: LiveData<List<Habitude>> = _selectedItems

    fun selectItem(item: Habitude) {
        _selectedItems.value = _selectedItems.value?.plus(item)
    }
}