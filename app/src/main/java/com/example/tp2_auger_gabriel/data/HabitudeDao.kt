package com.example.tp2_auger_gabriel.data

import androidx.lifecycle.LiveData
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.tp2_auger_gabriel.model.Habitude
@androidx.room.Dao
interface HabitudeDao {
    // Méthode suspendue pour insérer un utilisateur dans la base de données. L'utilisation de suspend indique
    // que cette opération doit être appelée depuis une coroutine ou une autre fonction suspendue, permettant une exécution asynchrone.
    @Insert
    suspend fun insertHabit(habitude: Habitude)

    // Insère un utilisateur et retourne l'ID généré pour cet utilisateur. Utile pour obtenir une confirmation de l'insertion.
    @Insert
    suspend fun insert(habitude: Habitude): Long

    // Méthode suspendue pour mettre à jour les informations d'un utilisateur existant.
    @Update
    suspend fun updateHabit(habitude: Habitude)

    // Méthode suspendue pour supprimer un utilisateur de la base de données.
    @Delete
    suspend fun delete(habitude: Habitude)

    @Query("SELECT * FROM habitudes WHERE id = :id")
    fun getHabitById(id: Int): LiveData<Habitude>

    @Query("SELECT * FROM habitudes")
    fun getAll(): LiveData<List<Habitude>>

    // Méthode suspendue pour supprimer tous les habitudes de la base de données. Comme cette opération
    // peut prendre du temps, elle est marquée comme suspend pour une exécution asynchrone.
    @Query("DELETE FROM habitudes")
    suspend fun deleteAllHabits()

}