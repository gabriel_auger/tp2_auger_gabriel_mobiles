package com.example.tp2_auger_gabriel.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.tp2_auger_gabriel.model.Habitude
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [Habitude::class], version = 1)
abstract class HabitudeDatabase : RoomDatabase() {
    abstract fun habitudeDao(): HabitudeDao

    companion object {
        @Volatile
        private var INSTANCE: HabitudeDatabase? = null

        fun getDatabase(context: Context): HabitudeDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    HabitudeDatabase::class.java,
                    "habitude_database"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }

    private class HabitudeDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch(Dispatchers.IO) {
                    populateDatabase(database.habitudeDao())
                }
            }
        }

        suspend fun populateDatabase(habitDao: HabitudeDao) {
            habitDao.deleteAllHabits()
            var habit = Habitude(1, "Habit 1")
            habitDao.insertHabit(habit)
            habit = Habitude(2, "Habit 2")
            habitDao.insertHabit(habit)
        }
    }
}

