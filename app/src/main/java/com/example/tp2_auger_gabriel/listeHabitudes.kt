package com.example.tp2_auger_gabriel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tp2_auger_gabriel.data.HabitudeDao
import com.example.tp2_auger_gabriel.data.HabitudeDatabase
import com.example.tp2_auger_gabriel.model.Habitude

/**
 * A fragment representing a list of Items.
 */
class listeHabitudes : Fragment() {
    private var dao: HabitudeDao? = null
    private var columnCount = 1
    val viewModel: ViewModelPartage by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dao = HabitudeDatabase.getDatabase(requireContext()).habitudeDao()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_liste_habitudes_list, container, false)

        // Initialize dao here
        dao = HabitudeDatabase.getDatabase(requireContext()).habitudeDao()

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                val habits = Habitude.populateData()

                adapter = HabitudesDispoRecyclerViewAdapter(values = habits, dao = dao!!, listener = object : HabitudesDispoRecyclerViewAdapter.OnItemClickListener {
                    override fun onItemClick(item: Habitude) {
                        item.choisie = !item.choisie
                        viewModel.selectItem(item)
                    }
                })
            }
        }
        return view
    }
}